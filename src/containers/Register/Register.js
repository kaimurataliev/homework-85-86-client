import React, {Component, Fragment} from 'react';
import {PageHeader, Form, FormGroup, Col, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import {registerUser} from "../../store/actions/userActions";
import FormElement from '../../components/UI/FormElement/FormElement';

class Register extends Component {

    state = {
        username: '',
        password: ''
    };

    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = (event) => {
        event.preventDefault();
        this.props.registerUser(this.state);
    };

    hasErrorForField = (fieldName) => {
        // if(this.props.error) {
        //     if(this.props.errors[fieldName]) {
        //         return true;
        //     }
        // }
        // return false;
        return this.props.error && this.props.error.errors[fieldName];
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Register new user</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>

                    <FormElement
                        propertyName="username"
                        title="Username"
                        type="text"
                        value={this.state.username}
                        changeHandler={this.inputChangeHandler}
                        placeholder="Enter username"
                        autoComplete="new-username"
                        error={this.hasErrorForField('username') && this.props.error.errors.username.message}
                    />

                    <FormElement
                        propertyName="password"
                        title="Password"
                        type="password"
                        value={this.state.password}
                        changeHandler={this.inputChangeHandler}
                        placeholder="Enter password"
                        autoComplete="new-password"
                        error={this.hasErrorForField('password') && this.props.error.errors.password.message}
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Register</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>

        )
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => {
    return {
        registerUser: (userData) => dispatch(registerUser(userData))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);