import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {fetchTracks, playSong} from "../../store/actions/tracksActions";
import {ListGroup, ListGroupItem, Label} from 'react-bootstrap';

import image from '../../assets/images/play.svg';

class Tracks extends Component {

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchTracks(id);
    }

    playSong = (event, song, token) => {
        event.preventDefault();
        this.props.playSong(song, token);
    };

    render() {
        return (
            <Fragment>
                <Label bsStyle="primary" style={{fontSize: '20px'}}>Tracks</Label>
                <ListGroup style={{marginTop: '20px'}}>
                    {this.props.tracks.map((track, index) => {
                        return (
                            <ListGroupItem key={index}>
                                <span style={{marginRight: '30px'}}>№ {track.number}</span>
                                <strong style={{marginRight: '30px'}}>{track.title}</strong>
                                <span style={{marginRight: '30px'}}>Artist: {track.album.performer.name}</span>
                                <button onClick={(event) => this.playSong(event, track._id, this.props.user.token)}>
                                    <img src={image} style={{width: '20px', height: '20px'}} alt="play"/>
                                </button>
                            </ListGroupItem>
                        )
                    })}
                </ListGroup>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        tracks: state.tracks.tracks,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchTracks: (id) => dispatch(fetchTracks(id)),
        playSong: (song, token) => dispatch(playSong(song, token))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);
