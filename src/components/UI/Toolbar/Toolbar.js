import React, {Fragment} from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Toolbar = ({user}) => (
    <Navbar bsStyle="inverse">
        <Navbar.Header>
            <Navbar.Brand>
                <LinkContainer to="/" exact><a>Music</a></LinkContainer>
            </Navbar.Brand>
            <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav pullRight>
                <LinkContainer to="/" exact>
                    <NavItem>Artists</NavItem>
                </LinkContainer>

                <LinkContainer to="/history" exact>
                    <NavItem>Track history</NavItem>
                </LinkContainer>

                {user

                    ?

                    <Fragment>
                        <LinkContainer to="/register" exact>
                            <NavItem>Hello, {user.username}</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/login" exact>
                            <NavItem>Logout</NavItem>
                        </LinkContainer>
                    </Fragment>

                    :

                    <Fragment>
                        <LinkContainer to="/register" exact>
                            <NavItem>Sign up</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/login" exact>
                            <NavItem>Login</NavItem>
                        </LinkContainer>
                    </Fragment>
                }

            </Nav>
        </Navbar.Collapse>
    </Navbar>
);

export default Toolbar;