import axios from '../../axios';

export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_ERROR = 'FETCH_ARTISTS_ERROR';

export const fetchArtistsSuccess = (artists) => {
    return {type: FETCH_ARTISTS_SUCCESS, artists}
};

export const fetchArtistsError = (error) => {
    return {type: FETCH_ARTISTS_ERROR, error};
};

export const fetchArtists = () => {
    return (dispatch) => {
        return axios.get('/artists')
            .then(response => {
                dispatch(fetchArtistsSuccess(response.data))
            }, error => {
                const errorObj = error.response ? error.response.data : "no internet";
                dispatch(fetchArtistsError(errorObj))
            })
    }
};

