import axios from '../../axios';

export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';

export const fetchAlbumsSuccess = (data) => {
    return {type: FETCH_ALBUMS_SUCCESS, data};
};

export const fetchAlbums = (id) => {
    return dispatch => {
        return axios.get(`/albums?artist=${id}`)
            .then(response => {
                dispatch(fetchAlbumsSuccess(response.data))
            })
    }
};

